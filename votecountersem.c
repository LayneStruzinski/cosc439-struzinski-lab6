#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>

sem_t sem;

int votes = 0;

void *voteCounter(){

    sem_wait(&sem);
    
    for(int i = 0; i < 10000000; i++){
        votes++;
    }
    
    sem_post(&sem);    
}

int main(){

    sem_init(&sem, 0, 1);
    
    pthread_t tid1, tid2, tid3, tid4, tid5;
    
    pthread_create(&tid1, NULL, voteCounter, NULL);
    pthread_create(&tid2, NULL, voteCounter, NULL);
    pthread_create(&tid3, NULL, voteCounter, NULL);
    pthread_create(&tid4, NULL, voteCounter, NULL);
    pthread_create(&tid5, NULL, voteCounter, NULL);
    
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    pthread_join(tid3, NULL);
    pthread_join(tid4, NULL);
    pthread_join(tid5, NULL);
    
    printf("Vote total is: %d\n", votes);
    
    return 0;
}

