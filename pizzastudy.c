#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdint.h>

#define Pizza 13
#define Slice 8

int pizzaSlice = 0;
int pizzaAmount = 0; 

bool hasSlice = true;
bool first = true;

sem_t semO;
sem_t semD;

pthread_mutex_t mutex;

void *Study(void *arg){
	
	int id = (intptr_t) arg;
	
	while (pizzaAmount < Pizza){
		
		if (pizzaSlice > 0){
		
			pthread_mutex_lock(&mutex);
			
			printf("Student %d eats slice %d from pizza %d\n", id, pizzaSlice, 	pizzaAmount);
			pizzaSlice--;
			
			if(hasSlice){
				
				sleep(rand() % 3);
				hasSlice = false;
			}
			
			pthread_mutex_unlock(&mutex);
		}
		else{
		
			if(first){
			
				sem_wait(&semO);
				printf("Student %d ordered pizza %d\n", id, pizzaAmount);
			
				first = false;
			}
		
		sleep(rand() % 3);
		sem_post(&semD);
		}
	}
}

void *Delivery(void *arg){ 

	while (pizzaAmount < Pizza){
	
		sem_wait(&semD);
		pthread_mutex_lock(&mutex);
		pizzaAmount++;
		
		pizzaSlice = Slice;
		
		printf("Pizza %d is delivered\n", pizzaAmount);
		
		pthread_mutex_unlock(&mutex);
		sem_post(&semO);
	}
}

int main(int argc, char *argv[]){
	
	sem_init(&semO, 0, 1);
	sem_init(&semD, 0, 1);
	
	pthread_mutex_init(&mutex, NULL);
	
	int n;
	n = atoi(argv[1]); 
	
	pthread_t tid1[n];
	pthread_t tid2[n];
	
	printf("Number of students: %d\n", n);
	
	for (int i = 1; i <= n; i++){
		
		pthread_create(&tid2[i], NULL, (void *)Study, (void *)(intptr_t) i);
		printf("I am Student %d\n", i);
	}
	for (int i = 1; i <= n; i++){
	
		pthread_create(&tid1[i], NULL, (void *)Delivery, (void *)(intptr_t) i);
	}
	
	
	for (int i = 0; i < n; i++){
		
		pthread_join(tid1[i], NULL);
		pthread_join(tid2[i], NULL);
	}
	
	sem_destroy(&semO);
	sem_destroy(&semD);
}

